import requests
import os
import shutil

def foo(n):
    sum = n*100.0//200
    return int(sum)

def add_percentages_to_levels(a, b):
    for x in a:
        x['percentages'] = 0
        for j in b.keys():
            if x['id'] == j:
                x['percentages'] = foo(b[j])
    return a

def favorite_image_url(a, url):
    var = a.find('media')
    new_string = url + a[var:]
    return new_string

def get_levels(url, page=None):
    # u = url + 'api/app'
    u = url + 'api/app/all'
    r = requests.get(u, stream=True)
    levels = [x for x in r.json() if x['approved']]
    levels_ = levels[:]
    # change the name of image field to 'name.png'
    if os.path.isdir('images'):
        for x in levels_: x['image'] = 'images/' + ''.join(x['image'].split('/').pop())
        if len(os.listdir('images')) == len(levels_):
            return [levels_, len(levels_)]
        else:
            print('deleting ...')
            shutil.rmtree('images')
            levels = [x for x in r.json() if x['approved']]
            return download_images(levels)
    else:
        return download_images(levels)

def download_images(levels):
    print('Downloading ...')
    os.mkdir('images')
    os.chdir('images')
    for x in levels:
        response = requests.get(x.get('image'), stream=True)
        with open(''.join(x.get('image').split('/').pop()), 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
        del response
    os.chdir('..')
    for x in levels: x['image'] = 'images/' + ''.join(x['image'].split('/').pop())
    return [levels, len(levels)]



def check_new_level():
    if os.path.isdir('images'):
        server_pic = []
        for x in self.data[0]:
            if x.get('image')[0] == 'h':
                server_pic.append(''.join(x.get('image')).split('/').pop())

        mob_pic = []
        for x in os.listdir('images'):
            mob_pic.append(x.partition('_')[2])

        if sorted(server_pic) == sorted(mob_pic):
            return 'it is OK!'
            #Clock.schedule_once(self.create_cards, 1)
        else:
            return 'deleting ...'
            #shutil.rmtree('images')
            #Clock.schedule_once(self.download_images, 20)
    else:
        #Clock.schedule_once(self.download_images, 20)
        return 'images dir is not found'
