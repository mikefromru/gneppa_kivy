import os, shutil, sys
import json
import threading
from kivy.storage.jsonstore import JsonStore
# os.chdir(os.path.dirname(os.path.realpath(__file__)))

from kivymd.app import MDApp
import kivy
kivy.require('2.1.0')

import sys, random
from kivy.config import Config

from kivy.animation import AnimationTransition
from kivy.metrics import dp
from kivy.animation import Animation

from kivy.factory import Factory
from kivymd.toast import toast

from kivy.utils import rgba

from kivy.uix.label import Label

from kivy.uix.screenmanager import ScreenManager, Screen

from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRectangleFlatButton, MDTextButton, MDRoundFlatButton, MDFlatButton, MDRaisedButton, MDIconButton
from kivymd.uix.snackbar import Snackbar
from kivymd.uix.datatables import MDDataTable
from kivy.uix.anchorlayout import AnchorLayout
from kivymd.uix.spinner import MDSpinner
from kivy.uix.recycleview import RecycleView
from kivymd.uix.card import MDCardSwipe
from kivymd.uix.list import (
    TwoLineAvatarIconListItem,
    OneLineListItem,
    OneLineAvatarIconListItem,
    TwoLineAvatarIconListItem,
    
)
from kivymd.uix.dialog import MDDialog
from kivymd.uix.list import IconLeftWidget
from kivymd.uix.gridlayout import GridLayout
from kivymd.uix.boxlayout import BoxLayout
from kivymd.uix.menu import MDDropdownMenu
from kivymd.theming import ThemableBehavior
from kivymd.uix.behaviors import RectangularElevationBehavior

from kivymd.uix.list import (
    OneLineIconListItem,
    OneLineRightIconListItem,
    OneLineAvatarListItem,
    IconLeftWidget,
    IconRightWidget,
    TwoLineIconListItem,
    OneLineAvatarIconListItem,
    TwoLineAvatarIconListItem,
    ThreeLineAvatarIconListItem
)

from kivy.utils import rgba
from kivy.metrics import dp

from kivy.config import Config
from kivy.core.window import Window
from kivy.core.text import LabelBase
from kivy.lang import Builder
from kivy.utils import platform

from kivy.clock import Clock

from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
    ListProperty,
    ColorProperty
)

import requests

from kivy.uix.screenmanager import (
    FadeTransition,
    NoTransition,
    WipeTransition,
    FallOutTransition,
    RiseInTransition,
    SlideTransition,
    CardTransition,
    SwapTransition
)

# my classes
from screens.detail import Detail
from screens.search import SearchScreen
from screens.settings import User_settings
from screens.favorite import FavoriteScreen
from screens.finish import FinishScreen
from screens.vocabulary import VocabularyScreen

# check OS
# It is one of: ‘win’, ‘linux’, ‘android’, ‘macosx’, ‘ios’ or ‘unknown’

from tools.local_settings import *
import tools.funcs as funcs

if platform == 'android':
    url = SERVER_URL
    ANDROID = True
else:
    url = LOCAL_URL
    ANDROID = False
    Window.size = (420, 820)
    Window.top = 10
    Window.left = 1090

with open('tools/url.txt', 'w') as o:
    o.write(url)

class WindowManager(ScreenManager):

    pass

#class RVItemLevel(OneLineAvatarIconListItem):
class RVItemLevel(MDCardSwipe):
    
    dialog = None
   
    mystar = StringProperty('False')
    text = StringProperty()
    #percentages = NumericProperty()
    percentages = StringProperty()
    levels = ListProperty()
    icon = StringProperty()
    image = StringProperty()
    open_my_card = False

    def __init__(self, **kwargs):
        super(RVItemLevel, self).__init__(**kwargs)
        self.config = App.get_running_app().config

    def close_dialog(self, *args):
        self.dialog.dismiss(force=True)

    def imageClick(self):
        pass

    def star(self):
        if self.mystar == 'True':
            ids_list = json.loads(self.config.get('Favorite', 'ids'))
            ids_list.remove(int(self.id))
            self.ids.star.icon = ''
            self.config.set('Favorite', 'ids', ids_list)
            self.config.write()


    def get_vocabulary(self):
        print('I am get_vocabulary')
        if self.open_my_card == True:
            print('I am get_vocabulary TRUE')
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='main')

            self.open_my_card = False
            VocabularyScreen.level = {'id': self.id, 'name': self.text}
            App.get_running_app().window_manager.current = 'vocabulary'
            App.get_running_app().window_manager.transition.direction = 'right'

    def click(self):
        print('I am click')
        if self.state == 'closed':
            self.open_my_card = False
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='main')
            Detail.level = {'slug': self.slug, 'name': self.text, 'id': self.id, 'image': self.image}
            App.get_running_app().window_manager.current = 'detail'
            App.get_running_app().window_manager.transition.direction = 'left'
        else:
            self.open_my_card = True


############## ############## Main ############## ############## ##############
class Main(Screen):

    flag = NumericProperty(0)
    levels = ObjectProperty()
    page = NumericProperty(1)
    total_levels = NumericProperty()
    total_pages = NumericProperty()
    slice_1, slice_2 = 0, 30
    pagination = 30
    i = 0

    def __init__(self, **kwargs):
        super(Main, self).__init__(**kwargs)
        tr = threading.Thread(target=self.thread_levels).start()
        self.event1 = Clock.schedule_interval(self.check_loading_levels, 5)
        self.loading = MDLabel(text='Loading...', halign='center')
        self.add_widget(self.loading)
        Window.bind(on_keyboard=self.goBackWindow)

        menu_items = [
            {
                "text": f"{i}",
                "viewclass": "OneLineListItem",
                "height": dp(56),
                "on_release": lambda x=f"{i}": self.menu_callback(x),
            } for i in ['Favorite', 'Settings']
        ]

        self.menu = MDDropdownMenu(
            items=menu_items,
            width_mult=2,
        )

    def goBackWindow(self, window, key, *args):
        if key == 27:
            if self.manager.current == 'main' and self.page > 1:
                self.prev_page(1)
                self.i = 0
            elif self.manager.current == 'main' and self.page == 1:
                if self.i == 1:
                    sys.exit()
                    return False
                else:
                    Snackbar(text='Click one more to close').open()
                    self.i += 1
            else:
                self.store = JsonStore("tools/store.json")
                screen_ = self.store.get('filebrowser')['screen']
                if screen_ == 'favorite':
                    self.manager.current = 'favorite'
                    self.store.put('filebrowser', screen='main')
                elif screen_ == 'search':
                    self.manager.current = 'search'
                    self.store.put('filebrowser', screen='main')
                else:
                    self.manager.current = 'main'
                    self.store.put('filebrowser', screen='main')
                self.manager.transition.direction = 'right'
            return True


    def callback_1(self, *args): # SearchScreen
        if self.levels:
            self.manager.current = 'search'
            App.get_running_app().window_manager.transition.direction = 'left'

    def callback_2(self, button): # show menu
        if self.levels:
            self.menu.caller = button
            self.menu.open()
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='main')

    def menu_callback(self, text_item):
        FavoriteScreen.levels = self.levels
        try:
            if text_item == 'favorite':
                print(self.levels)
            self.manager.current = text_item.lower()
            App.get_running_app().window_manager.transition.direction = 'left'
        except:
            pass
        self.menu.dismiss()

    def check_loading_levels(self, i):
        if self.levels:
            self.remove_widget(self.loading)
            self.event1.cancel()
            self.create_cards()

    def thread_levels(self):
        data = funcs.get_levels(url)
        self.levels = data[0]
        self.total_levels = data[1]


    def on_enter(self, **args):
        # update percantage labels if it comes from FinishScreen
        self.config = App.get_running_app().config
        try:
            store = JsonStore('tools/store.json')
            screen_ = store.get('filebrowser')['screen']
            rebootCard= store.get('filebrowser1')['rebootCard']
            if screen_ == 'finish' or screen_ == 'settings' or rebootCard == True:
                self.create_cards()
                store.put('filebrowser', screen='main')
                store.put('filebrowser1', rebootCard=False)
            else:
                #store.put('filebrowser', screen='main')
                pass
        except:
            pass


    def create_cards(self):
        print('<<<< CREATING CARDS >>>>')

        favorite = eval(self.config.get('Favorite', 'ids'))
        
        levels = self.levels[self.slice_1:self.slice_2]
        
        for x in levels:
            if x.get('id') in favorite:
                x['icon'] = 'True' 
            else:
                x['icon'] = 'False'


        if self.config.has_section('progress'):
            d = eval(self.config.get('progress', 'level_time'))
            funcs.add_percentages_to_levels(levels, d)
            self.rv.data = [
                {
                    'slug': x.get('slug'), 
                    'id': str(x.get('id')), 
                    'text': x.get('name'), 
                    'percentages': str(x.get('percentages')) + '%',
                    'image': x.get('image'),
                    'mystar': x.get('icon'),
                } for x in levels]
        else:
            self.rv.data = [
                {
                    'slug': x.get('slug'), 
                    'id': str(x.get('id')), 
                    'text': x.get('name'), 
                    'image': x.get('image'),
                    'mystar': x.get('icon'),

                } for x in levels]


        total  = ''.join(str(self.total_levels)[-1])
        if int(total) == 0:
            self.total_pages = self.total_levels // self.pagination
        else:
            self.total_pages = self.total_levels // self.pagination + 1

    def next_page(self, instance=1):
        if self.levels:
            if self.slice_2 < len(self.levels):
                self.page += instance
                self.slice_1 += self.pagination
                self.slice_2 += self.pagination
                self.create_cards()
                self.ids.rv.scroll_y = 1  # start from top_

    def prev_page_test(self):
        if self.slice_1 > 0:
            self.page -= 1
            self.slice_1 -= self.pagination
            self.slice_2 -= self.pagination
            self.create_cards()
            self.ids.rv.scroll_y = 1  # start from top_

    def prev_page(self, instance):
        if self.levels:
            if self.slice_1 > 0:
                self.page -= instance
                self.slice_1 -= self.pagination
                self.slice_2 -= self.pagination
                self.create_cards()
                self.ids.rv.scroll_y = 1  # start from top_


    def get_first_page(self):
        if self.levels:
            self.slice_1 = 0
            self.slice_2 = self.pagination
            self.create_cards()

            if self.page != 1:
                self.page = 1
                self.create_cards()
                self.ids.rv.scroll_y = 1 # start from top_

    def get_last_page(self):
        if self.levels:
            self.slice_2 = self.total_pages * self.pagination
            self.slice_1 = self.slice_2 - self.pagination

            self.create_cards()
            self.ids.rv.scroll_y = 1 # start from top_

            if self.page != int(self.total_pages):
                self.page = int(self.total_pages)
                self.create_cards()
                self.ids.rv.scroll_y = 1 # start from top_

    def get_middle_less_page(self):
        if self.levels:
            self.page = int(self.total_pages)//2//2
            self.ids.rv.scroll_y = 1 # start from top_

    def get_middle_more_page(self):
        if self.levels:
            self.spinner.active = True
            page_ = int(self.total_pages)//2//2
            self.page = int(self.total_pages) - page_
            self.ids.rv.scroll_y = 1 # start from top_

    def on_leave(self):
        self.i = 0

############## ############## ############## ##############
class App(MDApp):

    def on_start(self):
        if not self.config.has_option('Settings', 'sound'):
            self.config.set('Settings', 'sound', True)

    def build(self):

        templates = ('templates/index.kv',)
        config = self.config
        theme = config.get('Settings', 'theme')
        App.get_running_app().theme_cls.primary_palette = 'Blue'
        App.get_running_app().theme_cls.theme_style = theme

        for x in templates:
            root = Builder.load_file(x)
        self.window_manager = WindowManager()

        return self.window_manager

    def build_config(self, config):
        self.config.setdefaults(
            "User", {
                'username': '',
                'password': ''
            }
        ),
        self.config.setdefaults(
            "Settings", {
                'theme': 'Dark',
                'questions': 5,
                'minutes': 1,
            }
        )
        self.config.setdefaults(
            "Favorite", {'ids': []},
        )

if __name__ == '__main__':
    LabelBase.register(name='Roboto',
                      fn_regular='fonts/Roboto-Regular.ttf',
                      fn_italic='fonts/Roboto-Italic.ttf',
                      fn_bold='fonts/Roboto-Medium.ttf')
    App().run()
