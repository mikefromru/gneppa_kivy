from kivy.app import App
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.uix.button import MDFlatButton, MDRectangleFlatButton, MDRoundFlatButton
from kivymd.uix.dialog import MDDialog

from kivy.storage.jsonstore import JsonStore
from kivymd.uix.menu import MDDropdownMenu

from kivy.metrics import dp

from kivy.properties import (
    NumericProperty,
    StringProperty,
)

import os

class User_settings(Screen):

    dialog = None

    minutes = NumericProperty()
    questions = StringProperty()

    def __init__(self, **kwargs):
        super(User_settings, self).__init__(**kwargs)
        self.config = App.get_running_app().config
        Clock.schedule_once(self.foo, 0.1)

        self.menu_minutes = [
            {
                "text": f"{i}",
                "viewclass": "OneLineListItem",
                'height': dp(54),
                "on_release": lambda x=f"{i}": self.minutes_change(x),
            } for i in [1, 2, 5]
        ]


        self.menu_questions = [
            {
                "text": f"{i}",
                "viewclass": "OneLineListItem",
                'height': dp(54),
                "on_release": lambda x=f"{i}": self.questions_change(x),
            } for i in ['5', '7', '10'] # you can add 'All' to the list
        ]

    def foo(self, i):

        self.menu_minutes = MDDropdownMenu(
            caller=self.ids.minutes,
            items=self.menu_minutes,
            width_mult=1,
        )

        self.menu_questions = MDDropdownMenu(
            caller=self.ids.questions,
            items=self.menu_questions,
            width_mult=1,
        )

    def on_enter(self, *args):
        self.store = JsonStore('tools/store.json')
        self.questions = self.config.get('Settings', 'questions')
        self.minutes = self.config.getint('Settings', 'minutes')
        self.sound = self.config.get('Settings', 'sound')
        theme = self.config.get('Settings', 'theme')
        if theme == 'Dark':
            self.ids.set_bool.active = True
        if self.sound == 'True':
            self.ids.set_sound.active = True

    def minutes_change(self, insta):
        self.minutes = int(insta)
        self.config.set('Settings', 'minutes', int(insta))
        self.config.write()
        self.menu_minutes.dismiss()

    def questions_change(self, insta):
        self.questions = insta
        self.config.set('Settings', 'questions', insta)
        self.config.write()
        self.menu_questions.dismiss()

    def theme_change(self, insta):
        switch = self.ids.set_bool.active

        if switch:
            App.get_running_app().theme_cls.theme_style = 'Dark'
            self.config.set('Settings', 'theme', 'Dark')
        else:
            App.get_running_app().theme_cls.theme_style = 'Light'
            self.config.set('Settings', 'theme', 'Light')

        self.config.write()


    def dialog_close(self, *args):
        self.dialog.dismiss(force=True)

    def reset_progress(self, instance):
        if self.config.has_section('progress'):
            d = eval(self.config.get('progress', 'level_time'))
            self.config.set('progress', 'level_time', {})
            self.store.put('filebrowser', screen='settings')
            self.config.write()
            self.dialog_close()
        else:
            self.dialog_close()

    def clean_progress(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title="Reset progress levels?",
                buttons=[
                    MDFlatButton(
                        text="CANCEL",
                        on_release=self.dialog_close
                        #theme_text_color="Custom",
                        #text_color=self.theme_cls.primary_color,
                    ),
                    MDRoundFlatButton(
                        text="ACCEPT",
                        on_release=self.reset_progress,
                        theme_text_color="Primary",
                        #text_color='Error',
                        #text_color=self.theme_cls.primary_color,
                    ),
                ],
            )
        self.dialog.open()

    def set_sound(self, instance):
        switch = self.ids.set_sound.active
        if switch:
            self.config.set('Settings', 'sound', True)
        else:
            self.config.set('Settings', 'sound', False)
        self.config.write()


    def callback_1(self, *args):
        self.manager.current = 'main'
        self.manager.transition.direction = 'right'
