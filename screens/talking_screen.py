import os
import requests
from kivy.storage.jsonstore import JsonStore
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.uix.snackbar import Snackbar
from kivy.utils import platform
from kivy.core.audio import SoundLoader
from kivy.clock import Clock
import time
from kivy.app import App
from kivy.properties import (
    ListProperty,
    ObjectProperty,
    NumericProperty,
    StringProperty,
    BooleanProperty
)

if platform != 'android':
    from playsound import playsound

from screens.finish import FinishScreen

class TalkingScreen(Screen):

    level = ObjectProperty()
    questions = ObjectProperty()
    timer_pause = BooleanProperty(False)
    add_current = BooleanProperty(False)
    current = NumericProperty(1)
    len_questions = NumericProperty()
    counter = NumericProperty()

    def set_counter(self, *args):
        self.counter = self.config.getint('Settings', 'minutes') * 60
        self.ids.timer.text = time.strftime('%M:%S', time.gmtime(self.counter))

    def on_enter(self):
        self.store = JsonStore("tools/store.json")
        self.config = App.get_running_app().config
        self.sound_bool = self.config.get('Settings', 'sound')
        theme = self.config.get('Settings', 'theme')
        if theme == 'Light':
            self.ids.play.icon = 'i/play/play_black36dp.png'
        else:
            self.ids.play.icon = 'i/play/play_white36dp.png'

        with open('tools/url.txt') as o:
            self.url = o.read()
        len_questions_ = self.config.get('Settings', 'questions')
        try:
            self.len_questions = int(len_questions_)
        except ValueError:
            self.len_questions = len(self.questions)
        if self.len_questions > len(self.questions):
            self.len_questions = len(self.questions)
        self.set_counter(self)
        self.play(self)
        Clock.schedule_once(self.run_timer, 3)
        self.ids.rw.opacity = 1

    def run_timer(self, i):
        if self.add_current:
            self.current += 1
        if self.current > self.len_questions:
            FinishScreen.level = self.level
            self.manager.current = 'finish'
        self.ids.timer.opacity = 1
        self.even_timer = Clock.schedule_interval(self.timer, 1)
        self.even_show_next_button = Clock.schedule_once(self.show_next_button, self.counter // 2)
        self.add_current = True

    def demo_play(self, *args):
        self.sound = playsound('sounds/sound.ogg')

    def play(self, *args):
        self.check_sound = self.questions[0].get('robot_voice')
        if self.check_sound:
            url = self.url[:-1] + self.check_sound
            r = requests.get(url)
            with open('sounds/sound.ogg', 'wb') as o:
                o.write(r.content)
            if platform == 'android':
                self.sound = SoundLoader.load('sounds/sound.ogg')
                if self.sound:
                    Clock.schedule_once(self.set_active_play_button,
                        self.sound.length)
                    self.sound.play()
            else:
                Clock.schedule_once(self.set_active_play_button, 3)
                Clock.schedule_once(self.demo_play, 0)
        else:
            pass

        self.ids.play.disabled = True
        name = self.questions[0].get('name')
        self.ids.question.text = name + ' ?'
        self.ids.play.opacity = 1

    def timer(self, *args):
        path_over_sound = 'sounds/over.ogg'
        var = time.gmtime(self.counter)
        self.ids.timer.text = time.strftime('%M:%S', var)
        if self.current <= self.len_questions:
            if self.counter == 0:
                self.questions.pop(0) # remove it from list of questions
                self.ids.next_button.opacity = 0
                self.even_timer.cancel()
                if self.current >= self.len_questions:
                    # stop it
                    self.even_timer.cancel()
                else:
                    self.play(self)
                self.set_counter(self)
                self.ids.timer.opacity = 0
                Clock.schedule_once(self.run_timer, 3)
            
            elif self.counter == 3: # run warning sound 3 secends
                if self.sound_bool == 'True':
                    if platform == 'android':
                        SoundLoader.load(path_over_sound).play()
                    else:
                        playsound(path_over_sound)
                else:
                    print('hi from False')

            self.counter -= 1
        else:
            self.even_timer.cancel()

    def pause(self):
        if self.timer_pause:
            self.even_timer()
            self.even_show_next_button()
            self.timer_pause = False
        else:
            self.even_timer.cancel()
            self.even_show_next_button.cancel()
            self.timer_pause = True


    def set_active_play_button(self, i):
        self.ids.play.disabled = False

    def callback_1(self): # go to main or favorite

        self.manager.current = self.store.get('filebrowser')['screen']
        App.get_running_app().window_manager.transition.direction = 'right'

    def show_next_button(self, i):
        self.ids.next_button.opacity = 1

    def stop_timer(self, i):
        self.even_timer.cancel()

    def next(self):
        if self.current == self.len_questions:
            Snackbar(text='This is the last one').open()
        else:
            self.questions.pop(0)
            self.ids.next_button.opacity = 0
            self.even_timer.cancel()
            self.play(self)
            self.set_counter(self)
            self.ids.timer.opacity = 0
            Clock.schedule_once(self.run_timer, 3)

    def on_leave(self):
        self.set_counter(self)
        self.ids.timer.opacity = 0
        self.ids.next_button.opacity = 0

        if self.check_sound:
            try:
                self.sound.stop()
            except:
                pass

        Clock.schedule_once(self.stop_timer, 1.5)

        try:
            self.even_show_next_button.cancel()
        except:
            pass

        self.add_current = False
        self.current = 1
        self.timer_pause = False
        self.ids.question.text = ''
        self.ids.play.opacity = 0
        self.ids.rw.opacity = 0
