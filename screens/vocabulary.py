from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import NumericProperty, ObjectProperty
from tools.local_settings import *
import requests
from kivy.storage.jsonstore import JsonStore

from kivymd.uix.label import MDLabel
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.button import MDFlatButton, MDFloatingActionButton, MDIconButton

from kivymd.uix.list import (
    ThreeLineListItem,
    OneLineRightIconListItem,
    OneLineAvatarListItem,
    IconLeftWidget,
    IconRightWidget,
    TwoLineIconListItem,
    OneLineAvatarIconListItem,
    ThreeLineAvatarIconListItem
)

from kivy.utils import rgba
from kivy.metrics import dp


class VocabularyScreen(Screen):
    level = ObjectProperty()
    id = NumericProperty()

    def __init__(self, **kwargs):
        super(VocabularyScreen, self).__init__(**kwargs)
        self.loading = MDLabel(text='Loading ...', halign='center')
        self.add_widget(self.loading)

    def on_enter(self, *args):

        self.ids.head.text = 'Vocabulary to ' + self.level.get('name')
        self.btn = MDFloatingActionButton(
            icon='arrow-right', 
            pos_hint={"center_x": .8, 'center_y': .1},
            on_press=self.go_back,
        )
        self.add_widget(self.btn)


        url = open('tools/url.txt').read()
        response = requests.get(url + 'api/app/vocabulary/' + self.level.get('id'))

        if response.status_code == 200:

            for x in response.json():
                
                items = MDBoxLayout(
                    orientation='vertical',
                    adaptive_height=True,
                    spacing=dp(10),
                    #size_hint_y=None,
                    #height=dp(90)
                    padding=(dp(20), dp(30), dp(20), dp(0))
                )
                title = MDLabel(
                    text=x.get('name'),
                    adaptive_height=True,
                    theme_text_color='Custom',
                    font_name='fonts/Roboto-Regular',
                    #font_name: 'fonts/Roboto-Regular'
                    #font_size: '18sp'
                    #text_color='Primary',
                    text_color=rgba('#2196F3'),
                    font_style='H6',
                )

                description = MDLabel(
                    text='[i]Description[/i]: ' + x.get('description'),
                    markup=True,
                    adaptive_height=True,
                    font_name='fonts/Roboto-Regular',
                    theme_text_color='Hint',
                    font_style='Subtitle2',
                    #font_style='Caption'
                )

                example = MDLabel(
                    text='[i]Example[/i]: ' + x.get('example'),
                    markup=True,
                    adaptive_height=True,
                    theme_text_color='Primary',
                    font_name='fonts/Roboto-Regular',
                    #text_color=rgba('#2196F3'),
                    font_style='Subtitle2',
                )

                self.remove_widget(self.loading)

                items.add_widget(title)
                items.add_widget(description)
                items.add_widget(example)
                self.ids.box.add_widget(items)

        elif response.status_code == 404:
            self.remove_widget(self.loading)
            self.not_found = MDLabel(text='Not Found', halign='center')
            self.add_widget(self.not_found)
        else:
            self.sww = MDLabel(text='Something went wrong', halign='center')
            self.add_widget(self.sww)

    def go_back(self, instance):
        try:
            store = JsonStore("tools/store.json")
            self.manager.current = store.get('filebrowser')['screen']
        except:
            self.manager.current = 'main'
        self.manager.transition.direction = 'left'

    def on_leave(self, *args):
        self.remove_widget(self.btn)
        self.ids.box.clear_widgets()
        self.ids.head.text = ''
        self.loading = MDLabel(text='Loading ...', halign='center')
        self.add_widget(self.loading)
        try:
            self.remove_widget(self.not_found)
        except:
            pass
        try:
            self.remove_widget(self.sww)
        except:
            pass
