from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.image import Image
from kivy.properties import StringProperty, NumericProperty, ObjectProperty

from kivymd.uix.button import MDRectangleFlatIconButton, MDRoundFlatIconButton
from kivy.uix.widget import Widget
import random
import json
from kivy.storage.jsonstore import JsonStore

class FinishScreen(Screen):
    
    level = ObjectProperty()
    congrat = StringProperty()

    def on_enter(self):
        self.store = JsonStore("tools/store.json")
        self.config = App.get_running_app().config

        self.minutes = self.config.getint('Settings', 'minutes')
        self.questions_num = self.config.get('Settings', 'questions')
        self.total = int(self.minutes) * int(self.questions_num)

        self.img = Image(
            source='i/logo/gneppa_icon.png',
            size_hint_y= None,
            allow_stretch=True,
            pos_hint={'center_y': 1}
        )
        self.ids.my_image.add_widget(self.img)

        s = [
        'Well done!', 
        'Good job!', 
        'Keep working!',
        'Congratulations!',
        'You are doing a great job!',
        'Great job!'
        ]

        self.congrat = random.choice(s)

        if self.config.has_section('progress'):
            d = eval(self.config.get('progress', 'level_time'))

            try:
                if not d[int(self.level['id'])] >= 200:
                    d[int(self.level['id'])] += self.total
                    lbl = self.level['name'] + ' +' + str(self.total)
                    self.store.put('filebrowser', screen='finish')
                    self.config.set('progress', 'level_time', d)
                    print('d >= 200')
                elif d[int(self.level['id'])] == 200:
                    self.store.put('filebrowser', screen='finish')
                    lbl = self.level['name']
                    print('d == 200')
                else:
                    d[int(self.level['id'])] = 200
                    lbl = self.level['name']
                    self.store.put('filebrowser', screen='finish')
                    self.config.set('progress', 'level_time', d)
                    print('Set d = 200')
            except KeyError:
                d[int(self.level['id'])] = self.total
                lbl = self.level['name'] + ' +' + str(self.total)
                self.store.put('filebrowser', screen='finish')
                self.config.set('progress', 'level_time', d)
        else:
            lbl = self.level['name'] + ' +' + str(self.total)
            self.config.add_section('progress')
            self.config.set('progress', 'level_time', {int(self.level['id']):self.total})
            self.store.put('filebrowser', screen='finish')



        self.config.write()

        self.btn = MDRoundFlatIconButton(
            text=lbl,
            icon='trophy',
            pos_hint={'center_x': .5}
        )
        self.ids.get_button.add_widget(self.btn)
        self.btn.bind(on_release=self.callback)

    def callback(self, instance):

        self.ids.get_button.remove_widget(self.btn)
        self.congrat = ''
        self.manager.current = 'main'

        self.ids.my_image.remove_widget(self.img)
        
        
