from kivy.app import App
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore
from kivy.core.window import Window

from kivymd.uix.card import MDCardSwipe
from kivymd.uix.list import (
    TwoLineAvatarIconListItem,
    OneLineListItem,
    OneLineAvatarIconListItem
)
from kivy.clock import Clock
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
    ListProperty,
    ColorProperty
)
import requests
from screens.detail import Detail
from screens.vocabulary import VocabularyScreen

import json

import tools.funcs as funcs

#class RVItemSearch(OneLineAvatarIconListItem):
class RVItemSearch(MDCardSwipe):
    
    mystar = StringProperty('False')
    text = StringProperty()
    levels = ListProperty()
    image = StringProperty()
    percentages = StringProperty()
    open_my_card = False

    def star(self):
        if self.mystar == 'True':
            self.store = JsonStore("tools/store.json")
            self.config = App.get_running_app().config
            ids_list = json.loads(self.config.get('Favorite', 'ids'))
            ids_list.remove(int(self.id))
            self.ids.star.icon = ''
            self.config.set('Favorite', 'ids', ids_list)
            self.config.write()
            self.store.put('filebrowser1', rebootCard=True)

    def click(self):
        if self.state == 'closed':
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='search')
            self.open_my_card = False
            Detail.level = {'slug': self.slug, 'name': self.text, 'id': self.id, 'image': self.image}
            App.get_running_app().window_manager.current = 'detail'
            App.get_running_app().window_manager.transition.direction = 'left'
        else:
            self.open_my_card = True

    def get_vocabulary(self):
        if self.open_my_card == True:
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='search')
            self.open_my_card = False
            VocabularyScreen.level = {'id': str(self.id), 'name': self.text}
            App.get_running_app().window_manager.current = 'vocabulary'
            App.get_running_app().window_manager.transition.direction = 'right'

class SearchScreen(Screen):

    def __init__(self, **kwargs):
        super(SearchScreen, self).__init__(**kwargs)
        Window.bind(on_keyboard=self.goBackWindow)

    def on_leave(self):
        self.ids.name.text = ''
        self.rv.data = []

    def on_enter(self):
        self.config = App.get_running_app().config
        with open('tools/url.txt') as o:
            self.url = o.read()

    def goBackWindow(self, window, key, *args):
        if key == 13:
            self.callback_2(self)

    def callback_1(self):
       self.manager.current = 'main'
       self.manager.transition.direction = 'right'

    def callback_2(self, instance):
        favorite = eval(self.config.get('Favorite', 'ids'))
        instance = self.ids.name
        if len(instance.text) > 2:
            url_ = self.url + 'api/app/search/'
            payload = {'search': instance.text}
            r = requests.get(url_, params=payload)
            levels = r.json()

            levels_true = []
            for x in levels:
                if x['approved']:
                    levels_true.append(x)

            
            for x in levels_true:
                if x.get('id') in favorite:
                    x['icon'] = 'True' 
                else:
                    x['icon'] = 'False'

            if self.config.has_section('progress'):
                d = eval(self.config.get('progress', 'level_time'))
                funcs.add_percentages_to_levels(levels_true, d)
                self.rv.data = [
                    {
                        'id': x.get('id'),
                        'slug': x.get('slug'),
                        'percentages': str(x.get('percentages')) + '%',
                        'text': x.get('name'),
                        'image': 'images/' + x.get('image').split('/')[-1],
                        'mystar': x.get('icon')
                    } for x in levels_true]

            else:
                self.rv.data = [
                    {
                        'id': x.get('id'),
                        'slug': x.get('slug'),
                        'text': x.get('name'),
                        'image': 'images/' + x.get('image').split('/')[-1],
                        'mystar': x.get('icon')
                    } for x in levels_true]

            RVItemSearch.levels = levels_true
        else:
            pass
