
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.clock import Clock

from kivy.storage.jsonstore import JsonStore
from kivymd.uix.snackbar import Snackbar

from kivy.utils import rgba
from kivy.metrics import dp
import random
import requests
import json

from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.spinner import MDSpinner
from kivymd.uix.snackbar import Snackbar
from kivymd.uix.label import MDLabel
from kivymd.uix.gridlayout import GridLayout
from kivymd.uix.button import MDRectangleFlatButton, MDTextButton, MDRoundFlatButton, MDFlatButton, MDRaisedButton, MDIconButton

from kivy.app import App
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ObjectProperty,
    BooleanProperty,
    ListProperty,
    ColorProperty
)

from screens.talking_screen import TalkingScreen

class Detail(Screen):

    level = ObjectProperty()
    icon_color = ColorProperty()

    def __init__(self, **kwargs):
        super(Detail, self).__init__(**kwargs)
        self.loading = MDLabel(text="Loading ...", halign='center')
        self.add_widget(self.loading)

    def on_enter(self):
        self.store = JsonStore("tools/store.json")
        self.url = open('tools/url.txt').read()
        self.config = App.get_running_app().config
        self.ids.toolbar.title = self.level['name']
        ids_list = json.loads(self.config.get('Favorite', 'ids'))
        if len(ids_list) == 0:
            self.bar = self.ids.toolbar.right_action_items = [['star', lambda x: self.add_remove_favorite()]]
            for icon in self.ids.toolbar.ids.right_actions.children:
                icon.text_color = rgba('#000000')
        else:
            if int(self.level.get('id')) in ids_list:
                self.ids.toolbar.right_action_items = [['star', lambda x: self.add_remove_favorite()]]
                for icon in self.ids.toolbar.ids.right_actions.children:
                    icon.text_color = App.get_running_app().theme_cls.primary_color
            else:
                self.bar = self.ids.toolbar.right_action_items = [['star', lambda x: self.add_remove_favorite()]]
                for icon in self.ids.toolbar.ids.right_actions.children:
                    icon.text_color = rgba('#000000')

        slug = self.level['slug']
        self.r = requests.get(self.url + f'api/app/{slug}/')
        self.questions = self.r.json()
        self.questions = [x for x in self.questions if len(x.get('name')) < 61]

        number_questions_ = App.get_running_app().config.get('Settings', 'questions')
        try:
            number_questions = int(number_questions_)
        except ValueError:
            number_questions = len(self.questions)

        # get items that less then 60 symbols
        count = 0

        if len(self.questions) >= int(number_questions):
            self.random_questions = random.sample(self.questions, number_questions)
        else:
            self.random_questions = random.sample(self.questions, len(self.questions))

        for x in self.random_questions:
            grid = GridLayout(cols=2)
            grid.add_widget(MDIconButton(
                icon='circle', size_hint_x=None, width=dp(1),
                theme_text_color='Custom',
                text_color=App.get_running_app().theme_cls.primary_color,
                user_font_size=('10sp'),
                ))

            grid.add_widget(MDLabel(text=x.get('name') + ' ?', italic=True, theme_text_color='Primary', font_style='Subtitle2', markup=True))
            #grid.add_widget(MDLabel(text=x.get('name') + ' ?', font_style='Button'))
            self.ids.box.add_widget(grid)

        self.remove_widget(self.loading)
        self.ids.arrow_right.opacity = 1
        self.ids.arrow_right.disabled = False

    def add_remove_favorite(self, *args):
        id_level = int(self.level.get('id'))
        ids_list = json.loads(self.config.get('Favorite', 'ids'))
        if len(ids_list) == 0:
            ids_list.append(id_level)
            for icon in self.ids.toolbar.ids.right_actions.children:
                icon.text_color = App.get_running_app().theme_cls.primary_color
        elif len(ids_list) == 10:
            Snackbar(text='You can have only 10 favorite topics').open()
        else:
            if int(id_level) in ids_list:
                ids_list.remove(id_level)
                for icon in self.ids.toolbar.ids.right_actions.children:
                    icon.text_color = rgba('#000000')
            else:
                ids_list.append(id_level)
                for icon in self.ids.toolbar.ids.right_actions.children:
                    icon.text_color = App.get_running_app().theme_cls.primary_color
        self.store.put('filebrowser1', rebootCard=True)

        self.config.set('Favorite', 'ids', ids_list)
        self.config.write()

    def callback_1(self, *args): # go to preview screen


        self.manager.current = self.store.get('filebrowser')['screen']
        self.remove_widget(self.ids.arrow_right)
        Clock.schedule_once(self.create_arrow_right_button, 1)
        self.manager.transition.direction = 'right'

    def create_arrow_right_button(self, i):
        self.add_widget(self.ids.arrow_right)

    def lesson(self, *args):
        self.manager.current = 'talking_screen'
        TalkingScreen.questions = self.random_questions
        #TalkingScreen.level_name = self.level['name']
        TalkingScreen.level = self.level
        self.remove_widget(self.ids.arrow_right)
        Clock.schedule_once(self.create_arrow_right_button, 1)

    def on_leave(self):
        self.ids.box.clear_widgets()
        self.ids.toolbar.right_action_items = []
        self.ids.toolbar.title = ''
        self.ids.arrow_right.opacity = 0
        self.ids.arrow_right.disabled = True
        self.loading = MDLabel(text="Loading ...", halign='center')
        self.add_widget(self.loading)
