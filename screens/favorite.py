from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
import json
from kivymd.uix.label import MDLabel
from kivymd.uix.card import MDCardSwipe
from kivymd.uix.list import TwoLineAvatarIconListItem, OneLineListItem, OneLineAvatarIconListItem
from kivy.properties import (
    NumericProperty,
    StringProperty,
    ListProperty,
    ObjectProperty
)

from kivy.storage.jsonstore import JsonStore
from screens.detail import Detail
from screens.vocabulary import VocabularyScreen
import tools.funcs as funcs

class RVItemFavorite(MDCardSwipe):
#class RVItemFavorite(OneLineAvatarIconListItem):
    
    text = StringProperty()
    percentages = StringProperty()
    image = StringProperty()
    levels = ListProperty()
    del_id = NumericProperty()
    data_ = ListProperty()

    open_my_card = False

    def __init__(self, **kwargs):
        super(RVItemFavorite, self).__init__(**kwargs)
        self.config = App.get_running_app().config
        self.store = JsonStore("tools/store.json")

    def click(self):
        if self.state == 'closed':
            store = JsonStore("tools/store.json")
            store.put('filebrowser', screen='favorite')
            Detail.level = {'slug': self.slug, 'name': self.text, 'id': self.del_id}
            self.open_my_card = False
            App.get_running_app().window_manager.current = 'detail'
            App.get_running_app().window_manager.transition.direction = 'left'
        else:
            self.open_my_card = True

    def remove(self, instance):
        print(type(instance.spam))
        for x in self.data_:
            if x['del_id'] == instance.del_id:
                self.data_.remove(x)

        ids_list = json.loads(self.config.get('Favorite', 'ids'))
        ids_list.remove(instance.del_id)
        config = App.get_running_app().config
        if len(ids_list) == 0:
            ids_list = []
        self.config.set('Favorite', 'ids', ids_list)
        self.config.write()
        self.store.put('filebrowser1', rebootCard=True)

    def get_vocabulary(self):
        #if self.state == 'opened':
        if self.open_my_card == True:
            self.store = JsonStore("tools/store.json")
            self.store.put('filebrowser', screen='favorite')
            self.open_my_card = False
            VocabularyScreen.level = {'id': str(self.del_id), 'name': self.text}
            App.get_running_app().window_manager.current = 'vocabulary'
            App.get_running_app().window_manager.transition.direction = 'right'

class FavoriteScreen(Screen):

    levels = ObjectProperty()

    def __init__(self, **kwargs):
        super(FavoriteScreen, self).__init__(**kwargs)
        self.config = App.get_running_app().config

    def on_enter(self, *args):
        ids = json.loads(self.config.get('Favorite', 'ids'))
        if len(ids) > 0:
            favorite_levels = []
            for x in ids:
                for j in self.levels:
                    if x == j.get('id'):
                        favorite_levels.append(j)
            if self.config.has_section('progress'):
                d = eval(self.config.get('progress', 'level_time'))
                funcs.add_percentages_to_levels(favorite_levels, d)
                self.rv.data = [
                    {'slug': x.get('slug'),
                    'text': x.get('name'),
                    'percentages': str(x.get('percentages')) + '%',
                    'del_id': x.get('id'),
                    'image': x.get('image')} for x in favorite_levels]

            else:
                self.rv.data = [
                    {'slug': x.get('slug'),
                    'text': x.get('name'),
                    'del_id': x.get('id'),
                    'image': x.get('image')} for x in favorite_levels]

            RVItemFavorite.data_ = self.rv.data
        else:
            self.label = MDLabel(text='Empty', halign='center')
            self.add_widget(self.label)

    def on_leave(self, *args):
        self.rv.data = []
        try:
            self.remove_widget(self.label)
        except:
            pass

    def callback_1(self): # go back to main
        self.manager.current = 'main'
        self.manager.transition.direction = 'right'
